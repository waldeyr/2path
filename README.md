# 2Path #

2Path is a terpenoid metabolic network modeled and stored using a graph database, which aims to preserve important terpenoid biosynthesis characteristics and mitigate challenges in genome-scale storage metabolic networks.

Terpenoids have critical ecological, industrial and commercial relevance. Interactions as signaling for communication intra/inter species, signal molecules to attract pollinating insects, or defense against herbivores and microbes, are protagonized by terpenoids. Due to their chemical composition, many terpenoids possess vast pharmacological applicability in medicine and biotechnology. The biosynthesis of terpenes has been widely studied over the years and today it is known that they can be synthesized from two metabolic pathways: Mevalonate pathway (MVA) and Non-mevalonate pathway (MEP).

## How do I use it? ##

It is possible to use 2path in two ways:

Read only: online: www.2path.org

Reading and writing: setting up the localhost environment.

## How do I get set up? ##

### Dependencies ###

* Neo4j 3.1.3+ ([download and install](https://neo4j.com/download/other-releases/))
* Apache Tomcat ([install](http://tomcat.apache.org/))
* Maven ([install](https://maven.apache.org/install.html))

### Neo4J Configuration ###
Open the neo4.conf file

* To enable backups/dumps, uncomment this line:
dbms.allow_format_migration=true

* To disable authentication, uncomment this line
dbms.security.auth_enabled=false

### 2Path database Backup/Dump ###

There is a file called 2Path.dump in this git.

* backup
./neo4j-admin dump --database=graph.db --to=$HOME/2Path.dump

* restore
./neo4j-admin load --from=$HOME/2Path.dump --database=graph.db --force


## How to cite us? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
[Silva, Waldeyr, et al. "2Path: A terpenoid metabolic network modeled as graph database." Bioinformatics and Biomedicine (BIBM), 2016 IEEE International Conference on. IEEE, 2016.](http://ieeexplore.ieee.org/document/7822709)

@inproceedings{silva20162path,
  title={2Path: A terpenoid metabolic network modeled as graph database},
  author={Silva, Waldeyr and Vilar, Danilo and Souza, Daniel and Walter, Maria Em{\'\i}lia and Br{\'\i}gido, Marcelo and Holanda, Maristela},
  booktitle={Bioinformatics and Biomedicine (BIBM), 2016 IEEE International Conference on},
  pages={1322--1327},
  year={2016},
  organization={IEEE}
}
